import './App.css';
import { Link } from './components/Link';
import Students from './components/Students';
import { ContainerLinks } from './container/ContainerLinks';

function App() {

  return (
    <div className="App">
      <Students/>
      <ContainerLinks>
        <Link url={"www.firstChild"} text={"first Child"}/>
        <Link url={"www.secondChild"} text={"second Child"}/>
        <Link url={"www.thirdChild"} text={"third Child"}/>
        <Link url={"www.fourthChild"} text={"fourth Child"}/>
        <Link url={"www.fifthChild"} text={"fifth Child"}/>
      </ContainerLinks>
    </div>
  );
}

export default App;
