import React from "react";
import "./index.css";

const Button =({children, onClick, className, ...attr}) =>{
    return(
        <button className={className} {...attr} onClick={onClick}>{children}</button>
    )
}

export default Button;