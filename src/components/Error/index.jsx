import React from "react";
import "./index.css";

const Error = ({ messageError, className }) => {
  return <p className={className}>{messageError}</p>;
};

export default Error;
