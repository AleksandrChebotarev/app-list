import React from "react";
import "./index.css";

const Input = ({ onChange, className, ...attr }) => {
  return <input className={className} {...attr} onChange={onChange} />;
};

export default Input;
