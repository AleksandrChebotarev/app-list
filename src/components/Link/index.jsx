import React, { useRef } from "react";
import "./index.css";

export const Link = ({ url, text, className }) => {
  const refLink = useRef(null);

  const onClick = (e) => {
    e.preventDefault();
    console.log(refLink.current);
  };

  return (
    <a ref={refLink} href={url} className={className} onClick={onClick}>
      {text}
    </a>
  );
};
