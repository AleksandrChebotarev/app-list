import React from "react";
import Button from "../Button";
import "./index.css";

const ListStudents = ({ list, deleteStudent, className }) => {
  return (
    <ul className={"App-list-students"}>
      {list.map((student) => (
        <li className={"App-list-students__item"} key={student.id}>
          <Button className={className} onClick={() => deleteStudent(student.id)}>{student.name}</Button>
        </li>
      ))}
    </ul>
  );
};

export default ListStudents;
