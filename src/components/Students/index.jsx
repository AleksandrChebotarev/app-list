import React, { useState } from "react";
import Button from "../Button";
import Error from "../Error";
import Input from "../Input";
import ListStudents from "../ListStudents";
import WrapperMain from "../WrapperMain";
import { deleteItem, addItem, changeValue } from "../../utils";

const MESSAGE_ERROR = "Ошибка, пустое поле ввода! Введите имя!";

const Students = () => {
  const [students, setStudents] = useState([]);
  const [name, setName] = useState("");
  const [error, setError] = useState("");

  const onChange = (e) => {
    if (error) setError("");

    setName(changeValue(e.target.value));
  };

  const handleAdd = () => {
    if (!name || !name.trim()) return setError(MESSAGE_ERROR);
    setStudents(addItem(students, name));
    setName("");
  };

  const handleDelete = (id) => {
    setStudents(deleteItem(students, id));
  };

  return (
    <>
      {error && <Error className="App-error" messageError={error} />}
      <WrapperMain>
        <Input
          type={"text"}
          className={"App-input"}
          value={name}
          placeholder={"Введите имя"}
          onChange={onChange}
        />
        <Button type={"button"} className={"App-btn"} onClick={handleAdd}>
          Добавить
        </Button>
        <ListStudents list={students} className={"App-list-students__btn"} deleteStudent={handleDelete} />
      </WrapperMain>
    </>
  );
};

export default Students;
