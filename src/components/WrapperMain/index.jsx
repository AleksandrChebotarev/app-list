import React from "react";
import "./index.css";

const WrapperMain = ({ children }) => {
  return <main className={"App-wrapper"}>{children}</main>;
};

export default WrapperMain;
