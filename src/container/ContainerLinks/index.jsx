import { Children, cloneElement } from "react";
import "./index.css";

export const ContainerLinks = ({ children }) => {
  return (
    <nav className="App-contaiter-links">
      {Children.map(children, (child, index) =>
        index ? (
          cloneElement(child, { className: "App-link", id: index })
        ) : null
      )}
    </nav>
  );
};
