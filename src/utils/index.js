const deleteItem = (list, id) => list.filter((student) => student.id !== id);
const addItem = (list, name) => {
  const ID = list.length ? list[list.length - 1].id + 1 : 0;
  return list.concat({ id: ID, name });
};
const changeValue = (value) => value.replace(/[0-9]/g, "");

export { deleteItem, addItem, changeValue };
